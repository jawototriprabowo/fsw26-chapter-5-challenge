const express = require('express');
const router = express.Router();
const exampleController = require('./controller/examplecontroller');

router.get('/', exampleController.home);

router.get('/game', exampleController.game);

router.get('/bio', exampleController.bio);

router.post('/login',exampleController.login);

module.exports = router