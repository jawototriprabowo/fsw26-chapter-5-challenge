const path = require('path')

module.exports = {
 
    
    home: (req,res) => {
        return res.sendFile(path.join(__dirname,'../home/index.html'))
    },
    game: (req,res) => {
        return res.sendFile(path.join(__dirname,'../game/challenge.html'))
    },
    bio: (req,res) => {
        return res.send('name: Jawoto Tri Prabowo')
    },
    getProductById: (req,res) => {
        const id = req.params.id
        return res.send(`id is: ${id}`)
    },
    product: (req,res) => {
        return res.json({
            'id': 1,
            'name': 'buku cerita',
            'price': 15000
        })
    },
    login: (req,res) => {
        const dummyUser = {
            email : 'jawoto@gmail.com',
            pasword : '1234'
        }
        if(req.body.email === dummyUser.email && req.body.pasword === dummyUser.pasword) {
            return res.json({
                'message': 'login berhasil',
                'data': dummyUser
            }, 200)
        }

        return res.json({
            'message': 'password atau email salah'

        }, 400)

        
    }
}